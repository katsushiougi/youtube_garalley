var YOUTUBE = {};

YOUTUBE.movieList =[
	'6rWCVERJNEc',
	'D9IMG9yX454',
	'G-_U5AWs_QQ',
	'rxwUCT0CN9Y'
];

function onYouTubePlayerReady(playerid) {
	YOUTUBE.readyFlg = true;
	YOUTUBE.elem = document.getElementById('myytplayer');
	//YOUTUBE.elem.playVideo();
	$('#yList li:first-child a').click();
}

(function($, w, d){

YOUTUBE.$W = $(w);
YOUTUBE.readyFlg = false;

//YouTubeを表示するクラス BEGIN
function EmbedYouTube() {
	//YouTubeの表示設定
	this.replaceElemIdStr = 'main';
	this.widthStr = '560';
	this.heightStr = '315';
	this.swfVersionStr = '8';
	this.parObj = { allowScriptAccess: "always" };
	this.attObj = { id: "myytplayer" };
	this.swfUrlStr = '';

}

EmbedYouTube.prototype.getSwfUrlStr = function(n) {
	//YouTubeのURLを返す関数
	return 'http://www.youtube.com/v/' + YOUTUBE.movieList[n] + '?enablejsapi=1&playerapiid=ytplayer';
};

EmbedYouTube.prototype.embed = function(str) {

	//YouTubeを貼り付ける関数
	var _this = this;

	swfobject.embedSWF(
		str,
		_this.replaceElemIdStr,
		_this.widthStr,
		_this.heightStr,
		_this.swfVersionStr,
		null,
		null,
		_this.parObj,
		_this.attObj
	);

};
// END

var embedYouTube = new EmbedYouTube();
embedYouTube.embed(embedYouTube.getSwfUrlStr(0));


//YouTubeのサムネイルを表示させる
var $yList = $('#yList');
var all_list_str = '';
var h1 = $('h1')[0];

for (var i = 0; i < YOUTUBE.movieList.length; i++) {

	var the_youtube_id = YOUTUBE.movieList[i];

	//サムネイルタグをセット
	var li_str = '<li><a href="' + the_youtube_id + '"><img src="http://img.youtube.com/vi/' + the_youtube_id + '/default.jpg"></a></li>';
	all_list_str += li_str;
};

$yList[0].innerHTML = all_list_str;


$('#yList a').click(function(n){

	var $this = $(this);
	if(YOUTUBE.readyFlg) {

		var title_str = '';
		var id = $(this).attr('href');

		YOUTUBE.elem.loadVideoById(id);

		//タイトル取得
		var jsonURL = 'http://gdata.youtube.com/feeds/api/videos/' + id + '?v=2&alt=jsonc&callback=?';

		if($this.data('myData')) {
			h1.innerHTML = $this.data('myData').title;
		}
		else {
			//alert(jsonURL);
			$.getJSON(jsonURL, function(json) {

			 	$this.data('myData', {
			 		hasJSON: true,
			 		title: json.data.title
			 	});

				h1.innerHTML = json.data.title;
			});
		}

	}

	return false;

});





})(jQuery, window, document);